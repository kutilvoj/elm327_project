#include <Arduino.h>

int rpm = 1500;
int coolantTemp = 50;
int speed = 50;
int airTemp = 50;
int throttle = 50;
int timingAdv = 50;
int engineLoad = 50;
int manifoldPressure = 50;
bool pcMode = 0;
void setup() {
  Serial.begin(115200);
}

void loop() {
  if (Serial.available() > 0) {
        String command = Serial.readStringUntil('\n');
        if (command == "PC_MODE") {
            pcMode = true;
            Serial.println("PC_MODE_ENABLED");
        }
  }
  if(pcMode == 1){
    
  delay(5000);
  rpm = random(rpm * 0.9, rpm * 1.1 + 1);
  coolantTemp = random(coolantTemp * 0.9, coolantTemp * 1.1 + 1);
  speed = random(speed * 0.9, speed * 1.1 + 1);
  airTemp = random(airTemp * 0.9, airTemp * 1.1 + 1);
  throttle = random(throttle * 0.9, throttle * 1.1 + 1);
  timingAdv = random(timingAdv * 0.9, timingAdv * 1.1 + 1);
  engineLoad = random(engineLoad * 0.9, engineLoad * 1.1 + 1);
  manifoldPressure = random(manifoldPressure * 0.9, manifoldPressure * 1.1 + 1);

  if(rpm < 600) rpm = 600;
  if(rpm > 3000) rpm = 3000;
  if(coolantTemp < 10) coolantTemp = 10;
  if(coolantTemp > 60) coolantTemp = 60;
  if(speed < 10) speed = 10;
  if(speed > 60) speed = 60;
  if(airTemp < 10) airTemp = 10;
  if(airTemp > 60) airTemp = 60;
  if(throttle < 10) throttle = 10;
  if(throttle > 60) throttle = 60;
  if(timingAdv < 10) timingAdv = 10;
  if(timingAdv > 60) timingAdv = 60;
  if(engineLoad < 10) engineLoad = 10;
  if(engineLoad > 60) engineLoad = 60;
  if(manifoldPressure < 10) manifoldPressure = 10;
  if(manifoldPressure > 60) manifoldPressure = 60;

  Serial.println("START");
  Serial.print("RPM: "); Serial.println(rpm);
  Serial.print("Coolant Temp: "); Serial.println(coolantTemp);
  Serial.print("SPEED: "); Serial.println(speed);
  Serial.print("Air Temp: "); Serial.println(airTemp);
  Serial.print("Throttle: "); Serial.println(throttle);
  Serial.print("Timing Adv: "); Serial.println(timingAdv);
  Serial.print("Engine Load: "); Serial.println(engineLoad);
  Serial.print("MAP: "); Serial.println(manifoldPressure);
  Serial.println("END");

  
  }
 
}

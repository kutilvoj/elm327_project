import sys
import serial
import serial.tools.list_ports
import re
import pyqtgraph as pg
from PyQt5.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QWidget, QLabel, QPushButton, QHBoxLayout, QFileDialog
from PyQt5.QtCore import QThread, pyqtSignal, QDateTime
from PyQt5.QtGui import QIcon
import pyqtgraph.exporters

class SerialThread(QThread):
    new_data = pyqtSignal(str)

    def __init__(self, port, baud_rate):
        super().__init__()
        self.port = port
        self.baud_rate = baud_rate
        self.running = True
        self.in_block = False  # Flag to check if within START-END block
        self.buffer = ""  # Buffer to hold data between START and END

    def run(self):
        with serial.Serial(self.port, self.baud_rate, timeout=1) as ser:
            ser.write(b"PC_MODE\n")  # Send signal to switch to PC mode
            while self.running:
                line = ser.readline().decode('utf-8').strip()
                if line:
                    if line == "START":
                        self.in_block = True
                        self.buffer = ""  # Reset buffer
                    elif line == "END":
                        if self.in_block:
                            self.new_data.emit(self.buffer)
                            self.in_block = False
                    elif self.in_block:
                        self.buffer += line + "\n"

    def stop(self):
        self.running = False

class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.rpm_data = []
        self.temp_data = []
        self.speed_data = []
        self.air_temp_data = []
        self.throttle_data = []
        self.timing_adv_data = []
        self.engine_load_data = []
        self.map_data = []
        self.last_data_time = None
        self.mouse_inside = False
        self.initUI()
        self.connect_to_esp()

    def connect_to_esp(self):
        esp_port = self.find_esp_port()
        if esp_port:
            self.serial_thread = SerialThread(esp_port, 115200)
            self.serial_thread.new_data.connect(self.update_plot)
            self.serial_thread.start()
        else:
            print("ESP not found on any COM port.")

    def find_esp_port(self):
        ports = list(serial.tools.list_ports.comports())
        for p in ports:
            print(p.device, p.description)  # Výpis všech COM portů a jejich popisů pro ladění
            if 'USB-SERIAL CH340' in p.description:  # Adjust this based on your ESP's description
                return p.device
        return None

    def initUI(self):
        self.setWindowTitle('CAR->ELM327->ESP32->PC')
        self.setWindowIcon(QIcon('logo.ico'))  # Přidání ikony
        self.setGeometry(100, 100, 800, 600)
        layout = QVBoxLayout()
        central_widget = QWidget()
        central_widget.setLayout(layout)
        self.setCentralWidget(central_widget)

        # Graph setup
        self.graphWidget = pg.PlotWidget()
        self.graphWidget.setBackground('black')
        layout.addWidget(self.graphWidget)

        # Axis setup
        self.axis = pg.DateAxisItem(orientation='bottom')
        self.graphWidget.setAxisItems({'bottom': self.axis})

        # Setup the right axis for RPM
        self.graphWidget.getPlotItem().showAxis('right')
        self.rpm_axis = self.graphWidget.getPlotItem().getAxis('right')
        self.rpm_axis.setLabel('RPM', color='yellow')
        self.rpm_axis.setPen(pg.mkPen('yellow'))
       
        # Plot data on different axes
        self.rpm_plot = self.graphWidget.plot(pen=pg.mkPen('y', width=3), name="RPM")
        self.rpm_plot.setZValue(1)  # Ensure it is above other plots

        # Connect rpm plot to the new axis
        self.graphWidget.getPlotItem().getAxis('right').linkToView(self.graphWidget.getPlotItem().getViewBox())
        self.graphWidget.getPlotItem().getAxis('right').setScale(10)

        # Setup the pen for other data with correct axis
        self.temp_plot = self.graphWidget.plot(pen=pg.mkPen('r', width=3), name="Coolant Temp")
        self.speed_plot = self.graphWidget.plot(pen=pg.mkPen('g', width=3), name="Speed")
        self.air_temp_plot = self.graphWidget.plot(pen=pg.mkPen('b', width=3), name="Air Temp")
        self.throttle_plot = self.graphWidget.plot(pen=pg.mkPen('c', width=3), name="Throttle Position")
        self.timing_adv_plot = self.graphWidget.plot(pen=pg.mkPen('m', width=3), name="Timing Advance")
        self.engine_load_plot = self.graphWidget.plot(pen=pg.mkPen('w', width=3), name="Engine Load")
        self.map_plot = self.graphWidget.plot(pen=pg.mkPen('orange', width=3), name="MAP")

        # Add legend
        self.legend = self.graphWidget.addLegend()

        # Labels for current values and data rate
        self.rpm_label = QLabel('Current RPM: N/A RPM')
        self.temp_label = QLabel('Current Coolant Temperature: N/A °C')
        self.speed_label = QLabel('Current Speed: N/A km/h')
        self.air_temp_label = QLabel('Current Air Temperature: N/A °C')
        self.throttle_label = QLabel('Current Throttle Position: N/A %')
        self.timing_adv_label = QLabel('Current Timing Advance: N/A °')
        self.engine_load_label = QLabel('Current Engine Load: N/A %')
        self.map_label = QLabel('Current MAP: N/A kPa')
        self.data_rate_label = QLabel('Data Rate: N/A')
        layout.addWidget(self.rpm_label)
        layout.addWidget(self.temp_label)
        layout.addWidget(self.speed_label)
        layout.addWidget(self.air_temp_label)
        layout.addWidget(self.throttle_label)
        layout.addWidget(self.timing_adv_label)
        layout.addWidget(self.engine_load_label)
        layout.addWidget(self.map_label)
        layout.addWidget(self.data_rate_label)

        # Control buttons
        button_layout = QHBoxLayout()
        self.quit_button = QPushButton('Quit')
        self.quit_button.clicked.connect(self.close)
        button_layout.addWidget(self.quit_button)

        self.clear_button = QPushButton('Clear')
        self.clear_button.clicked.connect(self.clear_graphs)
        button_layout.addWidget(self.clear_button)
        
        self.export_button = QPushButton('Export')
        self.export_button.clicked.connect(self.export_graph)
        button_layout.addWidget(self.export_button)

        self.toggle_buttons = {}
        for plot_name, plot in [('RPM', self.rpm_plot), ('Coolant Temp', self.temp_plot), ('Speed', self.speed_plot),
                                ('Air Temp', self.air_temp_plot), ('Throttle Position', self.throttle_plot),
                                ('Timing Advance', self.timing_adv_plot), ('Engine Load', self.engine_load_plot),
                                ('MAP', self.map_plot)]:
            button = QPushButton(plot_name)
            button.setCheckable(True)
            button.setChecked(True)
            button.clicked.connect(lambda checked, p=plot: p.setVisible(checked))
            button_layout.addWidget(button)
            self.toggle_buttons[plot_name] = button

        layout.addLayout(button_layout)
        
        # Add items to the legend
        self.legend.addItem(self.rpm_plot, "RPM")
        self.legend.addItem(self.temp_plot, "Coolant Temp")
        self.legend.addItem(self.speed_plot, "Speed")
        self.legend.addItem(self.air_temp_plot, "Air Temp")
        self.legend.addItem(self.throttle_plot, "Throttle Position")
        self.legend.addItem(self.timing_adv_plot, "Timing Advance")
        self.legend.addItem(self.engine_load_plot, "Engine Load")
        self.legend.addItem(self.map_plot, "MAP")
        
        # Crosshair line
        self.vLine = pg.InfiniteLine(angle=90, movable=False, pen=pg.mkPen('w', width=1))
        self.graphWidget.addItem(self.vLine, ignoreBounds=True)
        self.graphWidget.scene().sigMouseMoved.connect(self.mouse_moved)
        self.graphWidget.setMouseTracking(True)
        self.graphWidget.enterEvent = self.enter_graph
        self.graphWidget.leaveEvent = self.leave_graph

    def update_plot(self, data):
        current_time = QDateTime.currentDateTime().toMSecsSinceEpoch() / 1000  # Convert to seconds
        if self.last_data_time is not None:
            data_rate = current_time - self.last_data_time
            self.data_rate_label.setText(f'Data Rate: {data_rate:.1f} seconds')
        self.last_data_time = current_time

        if not self.mouse_inside:
            if 'RPM:' in data:
                rpm = int(re.search(r'RPM: (\d+)', data).group(1))
                self.rpm_data.append((current_time, rpm / 10))  # Scale RPM data
                self.rpm_plot.setData([t[0] for t in self.rpm_data], [t[1] for t in self.rpm_data])
                self.rpm_label.setText(f'Current RPM: {rpm} RPM')
            if 'Coolant Temp:' in data:
                temp = int(re.search(r'Coolant Temp: (\d+)', data).group(1))
                self.temp_data.append((current_time, temp))
                self.temp_plot.setData([t[0] for t in self.temp_data], [t[1] for t in self.temp_data])
                self.temp_label.setText(f'Current Coolant Temperature: {temp} °C')
            if 'SPEED:' in data:
                speed = int(re.search(r'SPEED: (\d+)', data).group(1))
                self.speed_data.append((current_time, speed))
                self.speed_plot.setData([t[0] for t in self.speed_data], [t[1] for t in self.speed_data])
                self.speed_label.setText(f'Current Speed: {speed} km/h')
            if 'Air Temp:' in data:
                air_temp = int(re.search(r'Air Temp: (\d+)', data).group(1))
                self.air_temp_data.append((current_time, air_temp))
                self.air_temp_plot.setData([t[0] for t in self.air_temp_data], [t[1] for t in self.air_temp_data])
                self.air_temp_label.setText(f'Current Air Temperature: {air_temp} °C')
            if 'Throttle:' in data:
                throttle = int(re.search(r'Throttle: (\d+)', data).group(1))
                self.throttle_data.append((current_time, throttle))
                self.throttle_plot.setData([t[0] for t in self.throttle_data], [t[1] for t in self.throttle_data])
                self.throttle_label.setText(f'Current Throttle Position: {throttle} %')
            if 'Timing Adv:' in data:
                timing_adv = int(re.search(r'Timing Adv: (\d+)', data).group(1))
                self.timing_adv_data.append((current_time, timing_adv))
                self.timing_adv_plot.setData([t[0] for t in self.timing_adv_data], [t[1] for t in self.timing_adv_data])
                self.timing_adv_label.setText(f'Current Timing Advance: {timing_adv} °')
            if 'Engine Load:' in data:
                engine_load = int(re.search(r'Engine Load: (\d+)', data).group(1))
                self.engine_load_data.append((current_time, engine_load))
                self.engine_load_plot.setData([t[0] for t in self.engine_load_data], [t[1] for t in self.engine_load_data])
                self.engine_load_label.setText(f'Current Engine Load: {engine_load} %')
            if 'MAP:' in data:
                map_value = int(re.search(r'MAP: (\d+)', data).group(1))
                self.map_data.append((current_time, map_value))
                self.map_plot.setData([t[0] for t in self.map_data], [t[1] for t in self.map_data])
                self.map_label.setText(f'Current MAP: {map_value} kPa')

    def mouse_moved(self, evt):
        pos = evt  # Get the position of the mouse in scene coordinates
        if self.graphWidget.sceneBoundingRect().contains(pos):
            mousePoint = self.graphWidget.plotItem.vb.mapSceneToView(pos)
            self.vLine.setPos(mousePoint.x())
            x = mousePoint.x()
            if len(self.rpm_data) > 0 and self.mouse_inside:
                self.update_crosshair_label(x)

    def update_crosshair_label(self, x):
        closest_index = min(range(len(self.rpm_data)), key=lambda i: abs(self.rpm_data[i][0] - x))
        closest_time = self.rpm_data[closest_index][0]

        rpm = self.rpm_data[closest_index][1] * 10  # Convert back to original RPM
        temp = self.get_closest_data(self.temp_data, closest_time)
        speed = self.get_closest_data(self.speed_data, closest_time)
        air_temp = self.get_closest_data(self.air_temp_data, closest_time)
        throttle = self.get_closest_data(self.throttle_data, closest_time)
        timing_adv = self.get_closest_data(self.timing_adv_data, closest_time)
        engine_load = self.get_closest_data(self.engine_load_data, closest_time)
        map_value = self.get_closest_data(self.map_data, closest_time)

        self.rpm_label.setText(f'RPM: {rpm} RPM')
        self.temp_label.setText(f'Coolant Temp: {temp} °C')
        self.speed_label.setText(f'Speed: {speed} km/h')
        self.air_temp_label.setText(f'Air Temp: {air_temp} °C')
        self.throttle_label.setText(f'Throttle: {throttle} %')
        self.timing_adv_label.setText(f'Timing Adv: {timing_adv} °')
        self.engine_load_label.setText(f'Engine Load: {engine_load} %')
        self.map_label.setText(f'MAP: {map_value} kPa')

    def get_closest_data(self, data_list, time):
        if not data_list:
            return "N/A"
        closest_index = min(range(len(data_list)), key=lambda i: abs(data_list[i][0] - time))
        return data_list[closest_index][1]

    def enter_graph(self, event):
        super(MainWindow, self).enterEvent(event)
        self.mouse_inside = True
        self.clear_labels()

    def leave_graph(self, event):
        super(MainWindow, self).leaveEvent(event)
        self.mouse_inside = False
        self.update_labels_with_current_data()

    def clear_labels(self):
        self.rpm_label.setText('Current RPM: N/A RPM')
        self.temp_label.setText('Current Coolant Temperature: N/A °C')
        self.speed_label.setText('Current Speed: N/A km/h')
        self.air_temp_label.setText('Current Air Temperature: N/A °C')
        self.throttle_label.setText('Current Throttle Position: N/A %')
        self.timing_adv_label.setText('Current Timing Advance: N/A °')
        self.engine_load_label.setText('Current Engine Load: N/A %')
        self.map_label.setText('Current MAP: N/A kPa')

    def update_labels_with_current_data(self):
        if self.rpm_data:
            self.rpm_label.setText(f'Current RPM: {self.rpm_data[-1][1] * 10} RPM')  # Convert back to original RPM
        if self.temp_data:
            self.temp_label.setText(f'Current Coolant Temperature: {self.temp_data[-1][1]} °C')
        if self.speed_data:
            self.speed_label.setText(f'Current Speed: {self.speed_data[-1][1]} km/h')
        if self.air_temp_data:
            self.air_temp_label.setText(f'Current Air Temperature: {self.air_temp_data[-1][1]} °C')
        if self.throttle_data:
            self.throttle_label.setText(f'Current Throttle Position: {self.throttle_data[-1][1]} %')
        if self.timing_adv_data:
            self.timing_adv_label.setText(f'Current Timing Advance: {self.timing_adv_data[-1][1]} °')
        if self.engine_load_data:
            self.engine_load_label.setText(f'Current Engine Load: {self.engine_load_data[-1][1]} %')
        if self.map_data:
            self.map_label.setText(f'Current MAP: {self.map_data[-1][1]} kPa')

    def clear_graphs(self):
        self.rpm_data.clear()
        self.temp_data.clear()
        self.speed_data.clear()
        self.air_temp_data.clear()
        self.throttle_data.clear()
        self.timing_adv_data.clear()
        self.engine_load_data.clear()
        self.map_data.clear()

        self.rpm_plot.clear()
        self.temp_plot.clear()
        self.speed_plot.clear()
        self.air_temp_plot.clear()
        self.throttle_plot.clear()
        self.timing_adv_plot.clear()
        self.engine_load_plot.clear()
        self.map_plot.clear()

        self.clear_labels()

    def export_graph(self):
        file_dialog = QFileDialog()
        file_path, _ = file_dialog.getSaveFileName(self, "Save Graph", "", "PNG Files (*.png);;All Files (*)")
        if file_path:
            exporter = pg.exporters.ImageExporter(self.graphWidget.plotItem)
            exporter.export(file_path)

    def closeEvent(self, event):
        self.serial_thread.stop()
        self.serial_thread.wait()
        super().closeEvent(event)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    mainWin = MainWindow()
    mainWin.show()
    sys.exit(app.exec_())



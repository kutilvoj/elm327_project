#include "main.hpp"

// Definition of global variables
unsigned long lastCommandTime = 0;
const long commandInterval = 1000;
volatile int currentPage = 0; // Must be volatile because it is changed in ISR
bool pcMode = false;

void setup() {
    Serial.begin(115200);
    pinMode(BUTTON_PIN, INPUT_PULLUP);

    Wire.begin(SDA_PIN, SCL_PIN);
    if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {
        Serial.println(F("SSD1306 initialization failed"));
        for (;;); // Stays in an infinite loop if initialization fails
    }
    display.setTextSize(1);
    display.setTextColor(SSD1306_WHITE);

    print("ELM327_ESP32 communicator ------------by: VJK");
    delay(500);
    connect();
}

void loop() {
    handleButtonPress();
    if (Serial.available() > 0) {
        String command = Serial.readStringUntil('\n');
        if (command == "PC_MODE") {
            pcMode = true;
            print("PC Mode Enabled");
            Serial.println("PC_MODE_ENABLED");
        }
    }

    unsigned long currentMillis = millis();
    if (currentMillis - lastCommandTime >= commandInterval) {
        if (pcMode) {
            // Queries for PIDs used on all pages
            sendCommand("010C");
            delay(tBR);
            String rpmResponse = readResponse();

            sendCommand("010D");
            delay(tBR);
            String speedResponse = readResponse();

            sendCommand("0111");
            delay(tBR);
            String throttlePositionResponse = readResponse();

            sendCommand("0104");
            delay(tBR);
            String engineLoadResponse = readResponse();

            sendCommand("010B");
            delay(tBR);
            String mapResponse = readResponse();

            sendCommand("010E");
            delay(tBR);
            String timingAdvanceResponse = readResponse();

            sendCommand("0105");
            delay(tBR);
            String coolantTempResponse = readResponse();

            sendCommand("010F");
            delay(tBR);
            String airTempResponse = readResponse();

            // Processing responses
            int rpm = parseRPM(rpmResponse);
            int coolantTemp = parseTemperature(coolantTempResponse, "41 05");
            int speed = parseSpeed(speedResponse);
            int airTemp = parseTemperature(airTempResponse, "41 0F");
            int throttlePosition = parseThrottlePosition(throttlePositionResponse);
            int timingAdvance = parseTimingAdvance(timingAdvanceResponse);
            int engineLoad = parseEngineLoad(engineLoadResponse);
            int map = parseMAP(mapResponse);

            Serial.println(rpmResponse);
            Serial.println(speedResponse);
            Serial.println(throttlePositionResponse);
            Serial.println(engineLoadResponse);
            Serial.println(mapResponse);
            Serial.println(timingAdvanceResponse);
            Serial.println(coolantTempResponse);
            Serial.println(airTempResponse);

            // Displaying data
            Serial.println("START");
            Serial.print("RPM: "); Serial.println(rpm);
            Serial.print("Coolant Temp: "); Serial.println(coolantTemp);
            Serial.print("SPEED: "); Serial.println(speed);
            Serial.print("Air Temp: "); Serial.println(airTemp);
            Serial.print("Throttle: "); Serial.println(throttlePosition);
            Serial.print("Timing Adv: "); Serial.println(timingAdvance);
            Serial.print("Engine Load: "); Serial.println(engineLoad);
            Serial.print("MAP: "); Serial.println(map);
            Serial.println("END");

        } else {
            if (currentPage == 0) {
                sendCommand("010C");
                delay(tBR);
                String rpmResponse = readResponse();
                handleButtonPress();
                sendCommand("010D");
                delay(tBR);
                String speedResponse = readResponse();
                handleButtonPress();
                sendCommand("0111");
                delay(tBR);
                String throttlePositionResponse = readResponse();
                handleButtonPress();
                int rpm = parseRPM(rpmResponse);
                int speed = parseSpeed(speedResponse);
                int throttlePosition = parseThrottlePosition(throttlePositionResponse);

                printITEM("RPM: ", rpm, 0, 0, "RPM");
                printITEM("SPEED: ", speed, 0, 10, "km/h");
                printITEM("THROTTLE: ", throttlePosition, 0, 20, "%");
            } else if (currentPage == 1) {
                sendCommand("0104");
                delay(tBR);
                String engineLoadResponse = readResponse();
                handleButtonPress();
                sendCommand("010B");
                delay(tBR);
                String mapResponse = readResponse();
                handleButtonPress();
                sendCommand("010E");
                delay(tBR);
                String timingAdvanceResponse = readResponse();
                handleButtonPress();
                int engineLoad = parseEngineLoad(engineLoadResponse);
                int map = parseMAP(mapResponse);
                int timingAdvance = parseTimingAdvance(timingAdvanceResponse);

                printITEM("LOAD: ", engineLoad, 0, 0, "%");
                printITEM("MAP: ", map, 0, 10, "kPa");
                printITEM("TIMING: ", timingAdvance, 0, 20, "°");
            } else if (currentPage == 2) {
                display.fillRect(0, 20, 128, 10, BLACK);
                sendCommand("0105");
                delay(tBR);
                String coolantTempResponse = readResponse();
                handleButtonPress();
                sendCommand("010F");
                delay(tBR);
                String airTempResponse = readResponse();
                handleButtonPress();
                int coolantTemp = parseTemperature(coolantTempResponse, "41 05");
                int airTemp = parseTemperature(airTempResponse, "41 0F");

                printITEM("COOLANT: ", coolantTemp, 0, 0, "°C");
                printITEM("AIR TEMP: ", airTemp, 0, 10, "°C");
            }
        }

        lastCommandTime = currentMillis;
    }
    delay(50); // Short pause to improve stability
}

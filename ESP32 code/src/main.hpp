#ifndef MAIN_HPP
#define MAIN_HPP

#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include "BluetoothSerial.h"

// Definition of pins for I2C communication (SDA and SCL)
#define SDA_PIN 5
#define SCL_PIN 4
#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 32
#define OLED_RESET -1
#define tBR 10 // time between queries
#define BUTTON_PIN 35 // Button for page switching

BluetoothSerial SerialBT;
String deviceName = "OBDII";
const char* pin = "1234";
uint8_t macAddress[6] = {0x1C, 0xA1, 0x35, 0x69, 0x8D, 0xC5};
bool connected = false;

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

// Declaration of global variables
extern unsigned long lastCommandTime;
extern const long commandInterval;
extern volatile int currentPage;

void print(const char* text);
void printITEM(const char* text, int num, int hor, int ver, const char* units);
void sendCommand(String cmd);
void initialize();
void connect();
void querySupportedPIDs();

String readResponse();
int parseRPM(String response);
int parseTemperature(String response, String pid);
int parseSpeed(String response);
int parseThrottlePosition(String response);
int parseTimingAdvance(String response);
int parseEngineLoad(String response);
int parseMAP(String response);
void IRAM_ATTR handleButtonInterrupt();

#endif // MAIN_HPP


void print(const char* text) {
    display.clearDisplay();
    display.setCursor(0, 0);

    int cursorX = 0;
    int cursorY = 0;
    int maxWidth = 128; // Maximum width of the display

    String word = "";
    for (int i = 0; i <= strlen(text); i++) {
        char currentChar = text[i];
        if (currentChar == ' ' || currentChar == '\0') {
            int wordWidth = word.length() * 6; // Approximate width of the word
            // Check if the word fits on the current line
            if (cursorX + wordWidth >= maxWidth) {
                // Move to the next line
                cursorX = 0;
                cursorY += 10; // Move cursor down by 10 pixels
                display.setCursor(cursorX, cursorY);
            }
            display.print(word + " ");
            cursorX += wordWidth + 6; // Update the cursor position
            word = "";
        } else {
            word += currentChar;
        }

        // If end of text, print the last word
        if (currentChar == '\0') {
            display.print(word);
        }
    }
    
    display.display();
    delay(1000);
}



void printITEM(const char* text, int num, int hor, int ver, const char* units) {
  display.fillRect(hor, ver, 128, 10, BLACK);
  display.setCursor(hor, ver);
  display.println(text);
  display.display();
  display.setCursor(hor + (strlen(text) * 6), ver);
  display.print(num);
  
  if (strcmp(units, "°C") == 0) {
    display.print(" ");
    display.write(248); // ASCII code for the degree symbol
    display.print("C");
  } else if (strcmp(units, "°") == 0) {
    display.print(" ");
    display.write(248); // ASCII code for the degree symbol
  } else {
    display.print(" ");
    display.println(units);
  }
  
  display.display();
}

void sendCommand(String cmd) {
  SerialBT.println(cmd);
  delay(100); // Gives ELM time to process the command
}

void initialize() {
  Serial.println("Initialization");
  print("Initialization");
  sendCommand("ATZ"); // Reset ELM327
  delay(1000);
  sendCommand("ATE0"); // Turn off echo
  delay(1000);
  sendCommand("ATH1"); // Turn on headers for easier parsing
  delay(1000);
  sendCommand("ATSP0"); // Automatic protocol selection
  delay(1000);
  querySupportedPIDs(); // Query supported PIDs
  Serial.println("Successfully initialized");
  print("Successfully initialized");
  delay(500);
  display.clearDisplay();
  display.display();
}

void connect() {
  SerialBT.begin("ESP32test", true); // Name for ESP32 Bluetooth device
  SerialBT.setPin(pin);
  // Connecting to ELM327
  Serial.println("Connecting to ELM327");
  print("Connecting to ELM327");
  for (int i = 0; i < 5; i++) {
    connected = SerialBT.connect(macAddress);
    if (connected) {
      break;
    } else {
      Serial.println("Cannot connect to ELM327");
      print("Cannot connect to ELM327");
      delay(1000);
    }
  }
  if (connected) {
    Serial.println("Connected to ELM327!");
    print("Connected to ELM327!");
    initialize();
  } else {
    Serial.println("Failed to establish a stable connection.");
    print("Failed to establish a stable connection.");
    while (1);
  }
}

void querySupportedPIDs() {
  sendCommand("0100"); // Query supported PIDs
  delay(1000);
  String response = readResponse();
  Serial.println("Supported PIDs:");
  Serial.println(response);
}

int parseRPM(String response) {
  int index = response.indexOf("41 0C");
  if (index != -1 && response.length() >= index + 6) {
    String rpmString = response.substring(index + 6, index + 11);
    int rpmHigh = strtol(rpmString.substring(0, 2).c_str(), NULL, 16);
    int rpmLow = strtol(rpmString.substring(3, 5).c_str(), NULL, 16);
    return ((rpmHigh * 256) + rpmLow) / 4;
  }
  return -1;
}

int parseTemperature(String response, String pid) {
  int index = response.indexOf(pid);
  if (index != -1 && response.length() >= index + 6) {
    String tempString = response.substring(index + 6, index + 8);
    int tempValue = strtol(tempString.c_str(), NULL, 16);
    return tempValue - 40;
  }
  return -999;
}

int parseSpeed(String response) {
  int index = response.indexOf("41 0D");
  if (index != -1 && response.length() >= index + 6) {
    String speedString = response.substring(index + 6, index + 8);
    return strtol(speedString.c_str(), NULL, 16);
  }
  return -1;
}

int parseThrottlePosition(String response) {
  int index = response.indexOf("41 11");
  if (index != -1 && response.length() >= index + 6) {
    String throttleString = response.substring(index + 6, index + 8);
    int throttleValue = strtol(throttleString.c_str(), NULL, 16);
    return throttleValue * 100 / 255;
  }
  return -1;
}

int parseTimingAdvance(String response) {
  int index = response.indexOf("41 0E");
  if (index != -1 && response.length() >= index + 6) {
    String timingString = response.substring(index + 6, index + 8);
    int timingValue = strtol(timingString.c_str(), NULL, 16);
    return timingValue / 2 - 64;
  }
  return -1;
}

int parseEngineLoad(String response) {
  int index = response.indexOf("41 04");
  if (index != -1 && response.length() >= index + 6) {
    String loadString = response.substring(index + 6, index + 8);
    int loadValue = strtol(loadString.c_str(), NULL, 16);
    return loadValue * 100 / 255;
  }
  return -1;
}

int parseMAP(String response) {
  int index = response.indexOf("41 0B");
  if (index != -1 && response.length() >= index + 6) {
    String mapString = response.substring(index + 6, index + 8);
    return strtol(mapString.c_str(), NULL, 16);
  }
  return -1;
}

String readResponse() {
  String response = "";
  unsigned long timeout = millis() + 500;
  while (millis() < timeout) {
    while (SerialBT.available()) {
      char c = SerialBT.read();
      response += c;
      if (c == '>') return response;
    }
  }
  return response;
}

void handleButtonPress() {
  static unsigned long lastPressTime = 0;
  unsigned long currentTime = millis();
  if (digitalRead(BUTTON_PIN) == LOW) {
    if (currentTime - lastPressTime > 500) { // Button debounce
      currentPage = currentPage + 1;
      if (currentPage == 3) {currentPage = 0;}
      lastPressTime = currentTime;
    }
    while (digitalRead(BUTTON_PIN) == 0) {}
  }
}
